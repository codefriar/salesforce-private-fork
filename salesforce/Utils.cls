public class Utils {
 // refactored from: http://stackoverflow.com/questions/9369653/retrieve-salesforce-instance-url-instead-of-visualforce-instance
 // Returns the Salesforce Instance that is currently being run on,
 // e.g. na12, cs5, etc.
    public static String Instance {
        public get {
            if (Instance == null) {
            // Split up the hostname using the period as a delimiter there are 3 options:
                List<String> parts = System.URL.getSalesforceBaseUrl().getHost().replace('-api','').split('\\.');
            // (2) na12.salesforce.com      --- 3 parts, Instance is 1st part
                if (parts.size() == 3) Instance = parts[0];
            // (1) my--test1--nexus.cs0.visual.force.com  --- 5 parts, Instance is 2nd part
                else if (parts.size() == 5) Instance = parts[1];
            // (3) my.domain.salesforce.com    --- 4 parts, Instance is not determinable
                else Instance = null;
            } return Instance;
        } private set;
    }
}
// And you can then get the Salesforce base URL like this:
//    public static String GetBaseUrlForInstance() {
//        return 'https://' + Instance + '.salesforce.com';
//    }
